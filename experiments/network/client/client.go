package main

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
	"time"
)

// Params client params
type Params struct {
	Method     string
	URL        string
	Payload    interface{}
	TimeoutSec int64
	URLValue   map[string]interface{}
	Headers    map[string]string
}

func encodePayload(payload interface{}) (io.Reader, error) {
	if payload == nil {
		return nil, nil
	}
	b, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}
	return bytes.NewBuffer(b), nil
}

func createRequestByJSON(c *gin.Context, params Params, skipVerify bool) (resp *http.Response, err error) {

	body, err := encodePayload(params.Payload)
	if err != nil {
		return nil, err
	}
	if params.TimeoutSec < 1 {
		params.TimeoutSec = 5
	}

	client := &http.Client{
		Timeout:   time.Duration(params.TimeoutSec) * time.Second,
		Transport: nil,
	}

	req, err := http.NewRequest(params.Method, params.URL, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("content-type", "application/json")
	if c != nil {
		r, ok := c.Get("RequestID")
		if ok {
			req.Header.Set("kyash-request-id", r.(string))
		}
	}
	for k, v := range params.Headers {
		req.Header.Set(k, v)
	}

	return client.Do(req)
}

type issuing struct {
}

func CreateRequestByJSON(c *gin.Context, params Params) (*http.Response, error) {
	return createRequestByJSON(c, params, false)
}

func (i *issuing) CheckUpdateCardExpirationDateNeeded(c *gin.Context, walletID uint64) (*http.Response, error) {

	// Make REST call to Issuing API
	issuingServerURL := "http://0.0.0.0:8080/ping2"

	// Make GET request to Issuing API
	response, err := CreateRequestByJSON(c, Params{Method: "GET", URL: issuingServerURL, Payload: nil})
	if err != nil {
		return nil, err
	}

	return response, nil
}

func main() {
	issuing := &issuing{}
	issuing.CheckUpdateCardExpirationDateNeeded(nil, 1)
}
