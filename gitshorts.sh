# git unstage
gu() {
  local targets=$(unbuffer git status -s | grep -v "^ " | grep -v "??")
  [ -z "$targets" ] && return
  local selected=$(
    echo $targets \
      | grep -v "^ " \
      | fzf -m --preview-window right:70% \
        --preview='git -c color.status=always status -s | tac; \
          echo -e "\n---"; \
          echo {} | awk "{print \$2}" | xargs git diff --staged --color 2>/dev/null' \
      | awk "{print \$2}"
  )
  if [ -n $selected ]; then
    files=($(echo $selected))
    for file in "${files[@]}"; do
      git restore --staged $file
      echo "unstageed: $file"
    done
  fi
}

# git stash
gstash() {
  [ -z "$(git status -s)" ] && return
  local selected=$(
    unbuffer git status -s | grep -v "  " \
      | fzf -m --preview-window right:70% \
        --preview='git -c color.status=always status -s; \
          echo -e "\n---"; \
          echo {} | awk "{print \$2}" | xargs git diff --color 2>/dev/null' \
      | awk "{print \$2}"
  )
  if [ -n "$selected" ]; then
    files=($(echo $selected))
    for file in "${files[@]}"; do
      git stash push -u $file
    done
  fi
}
# git stash apply
gstash-apply() {
  local targets=$(git stash list)
  [ -z "$targets" ] && return
  local selected=$(
    echo $targets | awk "{print \$1}" | sed 's/://' \
      | fzf -m --preview-window right:70% \
        --preview='git stash show -u --name-status {}; echo -e "\n---"; git stash show -p {} --color'
  )
  if [ -n "$selected" ]; then
    stashes=($(echo $selected))
    for stash in "${stashes[@]}"; do
      git stash apply $stash
    done
  fi
}
# git stash drop
gstash-drop() {
  local targets=$(git stash list)
  [ -z "$targets" ] && return
  local selected=$(
    echo $targets | awk "{print \$1}" | sed 's/://' \
      | fzf -m --preview-window right:70% \
        --preview='git stash show -u --name-status {}; echo -e "\n---"; git stash show -p {} --color'
  )
  if [ -n "$selected" ]; then
    stashes=($(echo $selected))
    sorted_stashes=($(printf "%s\n" "${stashes[@]}" | sort -t '{' -k 2 -n | tac))
    for stash in "${sorted_stashes[@]}"; do
      git stash drop $stash
    done
  fi
}

# ブランチ切り替え
glb() {
  local targets=$(git --no-pager branch | grep -v "*")
  [ -z "$targets" ] && return
  local selected=$(echo $targets | fzf --preview 'git branch --color=always; echo -e "\n---"; git -c color.status=always status')
  if [ -n "$selected" ]; then
    branch=$(echo "$selected" | awk '{print $1}')
    git switch $branch
  fi
}

# ブランチを削除
glbd() {
  local targets=$(git --no-pager branch | grep -v "*")
  [ -z "$targets" ] && return
  local selected=$(echo $targets | fzf -m --preview 'git branch --color=always; echo -e "\n---"; git -c color.status=always status' | sed 's|.* ||')
  if [ -n "$selected" ]; then
    branches=($(echo $selected))
    for branch in "${branches[@]}"; do
      git branch -d $branch
    done
    git branch
  fi
}
# ブランチを強制削除
glbD() {
  local targets=$(git --no-pager branch | grep -v "*")
  [ -z "$targets" ] && return
  local selected=$(echo $targets | grep -v "*" | fzf -m --preview 'git branch --color=always; echo -e "\n---"; git -c color.status=always status' | sed 's|.* ||')
  if [ -n "$selected" ]; then
    branches=($(echo $selected))
    for branch in "${branches[@]}"; do
      git branch -D $branch
    done
    git branch
  fi
}
