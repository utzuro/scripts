#!/bin/bash

echo "suspendw: info: moving into suspend mode"
echo "suspendw: info: to stop: input \"wake_up\""

systemctl suspend

_input=""

wait()
{
	while [ 1 == 1 ]; do
		sleep 30s
		echo "suspendw: warning: was no input: suspending"
		sleep 1s
		systemctl suspend
	done
}

wait &
wait_func_id=${!}
echo "suspendw: info: wait function started with PID: $wait_func_id"

while [ 1 == 1 ]; do
	read _input
	if [ "$_input" == "wake_up" ]; then
		echo "suspendw: info: stopping suspend"
		kill $wait_func_id
		exit
	else
		echo "suspendw: error: to exir from suspend input: 'wake_up'"
	fi
done

