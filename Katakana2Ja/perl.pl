#!/usr/bin/env perl

use Getopt::Long;

my $input_file = '';
my $output_file = '';
my $list_f = '';
my $separator = '';

my @list = ();
my $list_len = 0;

GetOptions ('file-input=s' => \$input_file,
		'file=s' => \$output_file,
		'replaces-list-file=s' => \$list_f,
		'separator=s' => \$separator
);

open (INPUT_F, '<', $input_file) or die "error: could not open " . $input_file . " due to: " .$! . "\n";
open (OUTPUT_F, '<', $output_file) or die "error: could not open " . $output_file . " due to: " .$! . "\n";
open (LIST_F, '<', $list_f) or die "error: could not open " . $list_f . " due to: " .$! . "\n";

while (<LIST_F>)
{
		chomp($_);
		push (@list, [split ($separator, $_)]);
		$list_len++;
}

close (LIST_F);

print STDOUT ("replacements list:\n");

foreach my $i (1..$list_len)
{
		print STDOUT ($list[$i - 1][0] . " --> " . $list[$i - 1][1] . "\n");
}

print STDOUT ("\n\n");

while (<INPUT_F>)
{
		for ($i = 1; $i <= $list_len; $i++)
		{
				$_ =~ s/$list[$i-1][0]/$list[$i-1][1]/g;
				print STDOUT ($i-1 . " ('" . $list[$i-1][0] . "' --> '" . $list[$i-1][1] . "'): " . $_);
		}

		print OUTPUT_F ($_);
}

close (INPUT_F);
close (OUTPUT_F);
