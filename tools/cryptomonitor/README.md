# Crypto-monitors

## Description
Tools to monitor the crypto market.

## Tools
Run `./btcmonitor` to see current BTC price.

Run docker image to see chart ⇩
```sh
docker build -t btcchart .
docker run -it btcchart
```

## Notes
You can specify the crypto and date range in the Dockerfile.
Tool to show BTC price in the top bar of desktop is in progress.
